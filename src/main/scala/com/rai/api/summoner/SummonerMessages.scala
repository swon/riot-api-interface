package com.rai.api.summoner

case class GetObjectsByName(summonerNames: List[String])
case class GetObjectsById(summonerIds: List[Long])
case class GetSummonerMasteries(summonerIds: List[Long])
case class GetNames(summonerIds: List[Long])
case class GetSummonerRunes(summonerIds: List[Long])
