package com.rai.api.stats


case class AggregatedStatsDto(averageAssists: Option[Int], averageChampionsKilled: Option[Int],
                              averageCombatPlayerScore: Option[Int], averageNodeCapture: Option[Int],
                              averageNodeCaptureAssist: Option[Int], averageNodeNeutralize: Option[Int],
                              averageNodeNeutralizeAssist: Option[Int], averageNumDeaths: Option[Int],
                              averageObjectivePlayerScore: Option[Int], averageTeamObjective: Option[Int],
                              averageTotalPlayerScore: Option[Int], botGamesPlayed: Int,
                              killingSpree: Int, maxAssists: Option[Int], maxChampionsKilled: Int,
                              maxCombatPlayerScore: Option[Int], maxLargestCriticalStrike: Int,
                              maxLargestKillingSpree: Int, maxNodeCapture: Option[Int],
                              maxNodeCaptureAssist: Option[Int], maxNodeNeutralize: Option[Int],
                              maxNodeNeutralizeAssist: Option[Int], maxNumDeaths: Option[Int],
                              maxObjectivePlayerScore: Option[Int], maxTeamObjective: Option[Int],
                              maxTimePlayed: Int, maxTimeSpentLiving: Int, maxTotalPlayerScore: Option[Int],
                              mostChampionKillsPerSession: Int, mostSpellsCast: Int, normalGamesPlayed: Int,
                              rankedPremadeGamesPlayed: Int, rankedSoloGamesPlayed: Int, totalAssists: Int,
                              totalChampionKills: Int, totalDamageDealt: Int, totalDamageTaken: Int,
                              totalDeathsPerSession: Option[Int], totalDoubleKills: Int, totalFirstBlood: Int,
                              totalGoldEarned: Int, totalHeal: Int, totalMagicDamageDealt: Int, totalMinionKills: Int,
                              totalNeutralMinionsKilled: Int, totalNodeCapture: Option[Int],
                              totalNodeNeutralize: Option[Int], totalPentaKills: Int, totalPhysicalDamageDealt: Int,
                              totalQuadraKills: Int, totalSessionsLost: Int, totalSessionsPlayed: Int,
                              totalSessionsWon: Int, totalTripleKills: Int, totalTurretsKilled: Int,
                              totalUnrealKills: Int)

/** Ranked endpoint objects */
case class ChampionStatsDto(id: Int, stats: AggregatedStatsDto)
case class RankedStatsDto(champions: List[ChampionStatsDto], modifyDate: Long, summonerId: Long)

/** Summary endpoint objects */
case class PlayerStatsSummaryDto(aggregatedStats: AggregatedStatsDto, losses: Int, modifyDate: Long,
                                 playerStatSummaryType: String, wins: Int)
case class PlayerStatsSummaryListDto(playerStatSummaries: List[PlayerStatsSummaryDto], summonerId: Long)