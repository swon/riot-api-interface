package com.rai.api.game

case class GetMyRecentGames()
case class GetRecentGames(summonerId: Long)