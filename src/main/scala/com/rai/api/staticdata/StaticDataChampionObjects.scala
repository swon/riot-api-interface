package com.rai.api.staticdata

case class BlockItemDto(count: Int, id: Int)
case class BlockDto(items: List[BlockItemDto], recMath: Boolean, blockDtoType: String)
case class StatsDto(armor: Double, armorperlevel: Double, attackdamage: Double, attackdamageperlevel: Double,
                    attackrange: Double, attackspeedoffset: Double, attackspeedperlevel: Double, crit: Double,
                    critperlevel: Double, hp: Double, hpperlevel: Double, hpregen: Double, hpregenperlevel: Double,
                    movespeed: Double, mp: Double, mpperlevel: Double, mpregen: Double, mpregenperlevel: Double,
                    spellblock: Double, spellblockperlevel: Double)
case class SkinDto(id: Int, name: String, num: Int)
case class RecommendedDto(blocks: List[BlockDto], champion: String, map: String, mode: String, priority: Boolean,
                          title: String, recommendedType: String)
case class PassiveDto(description: String, image: ImageDto, name: String, sanitizedDescription: String)
case class InfoDto(attack: Int, defense: Int, difficulty: Int, magic: Int)

/** @note 'effect': List of List of Double - may contain Nulls */
/** @note 'range': Either a List of Integer or the String 'self' for spells that target one's own champion */
case class ChampionSpellDto(altimages: List[ImageDto], cooldown: List[Double], cooldownBurn: String, cost: List[Int],
                            costBurn: String, costType: String, description: String, effect: List[Option[List[Double]]],
                            effectBurn: List[String], image: ImageDto, key: String, leveltip: LevelTipDto, maxrank: Int,
                            name: String, range: Either[List[Int], String], rangeBurn: String, resource: String,
                            sanitizedDescription: String, sanitizedTooltip: String, tooltip: String,
                            vars: List[SpellVarsDto])

/** @note if (champdata != 'all') then {id, key, name, title} */
case class ChampionDto(allytips: Option[List[String]], blurb: Option[String], enemytips: Option[List[String]], id: Int,
                       image: Option[ImageDto], info: Option[InfoDto], key: String, lore: Option[String], name: String,
                       partype: Option[String], passive: Option[PassiveDto], recommended: Option[List[RecommendedDto]],
                       skins: Option[List[SkinDto]], spells: Option[List[ChampionSpellDto]], stats: Option[StatsDto],
                       tags: Option[List[String]], title: String)

/** @note if (champdata != 'all') then {type, version, data} */
case class ChampionListDto(data: Map[String, ChampionDto], format: Option[String], keys: Option[Map[String, String]],
                           listType: String, version: String)
