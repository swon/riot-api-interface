package com.rai.api.lolmatch

import akka.actor.ActorRef
import com.rai.api.{RiotRetriever, Parcel, RiotApi}
import org.json4s._
import org.json4s.jackson.JsonMethods._
import uk.co.robinmurphy.http.Response


class MatchApi extends RiotApi {
  implicit val formats = org.json4s.DefaultFormats

  def receive = {
    case GetMatch(matchId, includeTimeline) => getMatch(matchId, includeTimeline)
    case Parcel(destination, res) => returnResponse(destination, res)
  }

  def returnResponse(destination: ActorRef, res: Response) = {
    if (res.statusCode < 300 && res.statusCode >= 200) {
      val json = parse(res.body)

      /** Hack to allow me to save matches */
      val matchJson = (json \ "mapId").toOption
      println("JSON: " + res.body)
      matchJson.foreach { s => sendToEs(res, compact(render(json \ "matchId")), esConf.getString("es.type.aram")) }
      json.extractOpt[MatchDetail].foreach {
        destination ! _
      }
    }
  }

  def getMatch(matchId: Long, includeTimeline: Boolean) = {
    val url = baseUri + "/api/lol/" + region + "/v2.2/match/" + matchId.toString
    if (includeTimeline)
      params += ("includeTimeline" -> "true")
    RiotRetriever.getData(sender, self, url, params)
  }
}
