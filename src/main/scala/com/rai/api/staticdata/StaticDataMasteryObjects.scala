package com.rai.api.staticdata

case class MasteryTreeItemDto(masteryId: Int, prereq: String)
case class MasteryTreeListDto(masteryTreeItems: List[MasteryTreeItemDto])
case class MasteryTreeDto(Defense: List[MasteryTreeListDto], Offense: List[MasteryTreeListDto],
                          Utility: List[MasteryTreeListDto])

/** @note if (masteryListData != 'all') then {id, name, description} */
case class MasteryDto(description: List[String], id: Int, image: Option[ImageDto], masteryTree: Option[String],
                      name: String, prereq: Option[String], ranks: Option[Int],
                      sanitizedDescription: Option[List[String]])

/** @note if (masteryListData != 'all') then {type, version, data} */
case class MasteryListDto(data: Map[String, MasteryDto], tree: Option[MasteryTreeDto], masteryListType: String,
                          version: String)

