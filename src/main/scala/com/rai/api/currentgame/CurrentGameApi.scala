package com.rai.api.currentgame

import akka.actor.ActorRef
import com.rai.api.{RiotRetriever, Parcel, RiotApi}
import org.json4s._
import org.json4s.jackson.JsonMethods._
import uk.co.robinmurphy.http.Response


class CurrentGameApi extends RiotApi {
  implicit val formats = org.json4s.DefaultFormats

  def receive = {
    case GetCurrentGameFor(platformId, summonerId) => getCurrentGameFor(platformId, summonerId)
    case Parcel(destination, res) => returnResponse(destination, res)
  }

  def returnResponse(destination: ActorRef, res: Response) = {
    val json = parse(res.body)
    json.extractOpt[CurrentGameInfo].foreach { destination ! _ }
  }

  def getCurrentGameFor(platformId: String, summonerId: Long) = {
    val url = baseUri + "/observer-mode/rest/consumer/getSpectatorGameInfo/" + platformId + "/" + summonerId.toString
    RiotRetriever.getData(sender, self, url, params)
  }
}
