package com.rai.api.currentgame

case class GetCurrentGameFor(platformId: String, summonerId: Long)
