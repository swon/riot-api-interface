package com.rai.api.team

import akka.actor.ActorRef
import com.rai.api.{RiotRetriever, Parcel, RiotApi}
import org.json4s._
import org.json4s.jackson.JsonMethods._
import uk.co.robinmurphy.http.Response


/** Since I dont use begin/endTime or begin/endIndex yet, I haven't implemented them */
class TeamApi extends RiotApi {
  implicit val formats = org.json4s.DefaultFormats
  val teamUrl = baseUri + "/api/lol/" + region + "/v2.4/team/"

  def receive = {
    case GetTeamsBySummoner(summonerIds) => getTeamsBySummoner(summonerIds)
    case GetTeams(teamIds) => getTeams(teamIds)
    case Parcel(destination, res) => returnResponse(destination, res)
  }

  def returnResponse(destination: ActorRef, res: Response) = {
    val json = parse(res.body)
    json.extractOpt[Map[String, List[TeamDto]]].foreach { destination ! _ }
    json.extractOpt[Map[String, TeamDto]].foreach { destination ! _ }
  }

  def getTeamsBySummoner(summonerIds: List[Long]) = {
    val url = teamUrl + "by-summoner/" + summonerIds.mkString(",")
    RiotRetriever.getData(sender, self, url, params)
  }

  def getTeams(teamIds: List[Long]) = {
    val url = teamUrl + teamIds.mkString(",")
    RiotRetriever.getData(sender, self, url, params)
  }
}
