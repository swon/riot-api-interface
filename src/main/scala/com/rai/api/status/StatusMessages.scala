package com.rai.api.status

case class GetStatus()
case class GetStatusFor(region: String)
