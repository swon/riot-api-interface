package com.rai.api.currentgame

case class BannedChampion(championId: Long, pickTurn: Int, teamId: Long)
case class CurrentGameParticipant(bot: Boolean, championId: Long, masteries: List[Mastery], profileIconId: Long,
                                  runes: List[Rune], spell1Id: Long, spell2Id: Long, summonerId: Long,
                                  summonerName: String, teamId: Long)
case class Mastery(masteryId: Long, rank: Int)
case class Observer(encryptionKey: String)
case class Rune(count: Int, runeId: Long)
case class CurrentGameInfo(bannedChampions: List[BannedChampion], gameId: Long, gameLength: Long, gameMode: String,
                           gameQueueConfigId: Long, gameStartTime: Long, gameType: String, mapId: Long,
                           observers: Observer, participants: List[CurrentGameParticipant], platformId: String)
