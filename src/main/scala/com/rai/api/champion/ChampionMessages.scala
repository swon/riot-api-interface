package com.rai.api.champion

case class GetAll(freeToPlay: Boolean = false)
case class GetById(id: Int)