package com.rai.api.featuredgames

case class BannedChampion(championId: Long, pickTurn: Int, teamId: Long)
case class Observer(encryptionKey: String)
case class Participant(bot: Boolean, championId: Long, profileIconId: Long, spell1Id: Long, spell2Id: Long,
                       summonerName: String, teamId: Long)
case class FeaturedGameInfo(bannedChampions: List[BannedChampion], gameId: Long, gameLength: Long, gameMode: String,
                            gameQueueConfigId: Long, gameStartTime: Long, gameType: String, mapId: Long,
                            observers: Observer, participants: List[Participant], platformId: String)
case class FeaturedGames(clientRefreshInterval: Long, gameList: List[FeaturedGameInfo])
