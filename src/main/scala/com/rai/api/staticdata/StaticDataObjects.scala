package com.rai.api.staticdata

case class SpellVarsDto(coeff: List[Double], dyn: String, key: String, link: String, ranksWith: String)
case class LevelTipDto(effect: List[String], label: List[String])
case class MetaDataDto(isRune: Boolean, tier: String, metadataType: String)
case class GoldDto(base: Int, puchaseable: Boolean, sell: Int, total: Int)
case class BasicDataStatsDto(FlatArmorMod: Double, FlatAttackSpeedMod: Double, FlatBlockMod: Double,
                             FlatCritChanceMod: Double, FlatCritDamageMod: Double, FlatEXPBonus: Double,
                             FlatEnergyPoolMod: Double, FlatEnergyRegenMod: Double, FlatHPPoolMod: Double,
                             FlatHPRegenMod: Double, FlatMPPoolMod: Double, FlatMPRegenMod: Double,
                             FlatMagicDamageMod: Double, FlatMovementSpeedMod: Double, FlatPhysicalDamageMod: Double,
                             FlatSpellBlockMod: Double, PercentArmorMod: Double, PercentAttackSpeedMod: Double,
                             PercentBlockMod: Double, PercentCritChanceMod: Double, PercentCritDamageMod: Double,
                             PercentDodgeMod: Double, PercentEXPBonus: Double, PercentHPPoolMod: Double,
                             PercentHPRegenMod: Double, PercentLifeStealMod: Double, PercentMPPoolMod: Double,
                             PercentMPRegenMod: Double, PercentMagicDamageMode: Double, PercentMovementSpeedMod: Double,
                             PercentPhysicalDamageMod: Double, PercentSpellBlockMod: Double,
                             PercentSpellVampMod: Double, rFlatArmorModPerLevel: Double,
                             rFlatArmorPenetrationMod: Double, rFlatArmorPenetrationModPerLevel: Double,
                             rFlatCritChanceModPerLevel: Double, rFlatCritDamageModPerLevel: Double,
                             rFlatDodgeMod: Double, rFlatDodgeModPerLevel: Double, rFlatEnergyModPerLevel: Double,
                             rFlatEnergyRegenModPerLevel: Double, rFlatGoldPer10Mod: Double, rFlatHPModPerLevel: Double,
                             rFlatHPRegenModPerLevel: Double, rFlatMPModPerLevel: Double,
                             rFlatMPRegenModPerLevel: Double, rFlatMagicDamageModPerLevel: Double,
                             rFlatMagicPenetrationMod: Double, rFlatMagicPenetrationModPerLevel: Double,
                             rFlatMovementSpeedModPerLevel: Double, rFlatPhysicalDamageModPerLevel: Double,
                             rFlatSpellBlockModPerLevel: Double, rFlatTimeDeadMod: Double,
                             rFlatTimeDeadModPerLevel: Double, rPercentArmorPenetrationMod: Double,
                             rPercentArmorPenetrationModPerLevel: Double, rPercentAttackSpeedModPerLevel: Double,
                             rPercentCooldownMod: Double, rPercentCooldownModPerLevel: Double,
                             rPercentMagicPenetrationMod: Double, rPercentMagicPenetrationModPerLevel: Double,
                             rPercentMovementSpeedModPerLevel: Double, rPercentTimeDeadMod: Double,
                             rPercentTimeDeadModPerLevel: Double)
case class BasicDataDto(colloq: String, consumeOnFull: Boolean, consumed: Boolean, depth: Int, description: String,
                        from: List[String], gold: GoldDto, group: String, hideFromAll: Boolean, id: Int,
                        image: ImageDto, inStore: Boolean, into: List[String], maps: Map[String, Boolean], name: String,
                        plaintext: String, requiredChampion: String, rune: MetaDataDto, sanitizedDescription: String,
                        specialRecipe: Int, stacks: Int, stats: BasicDataStatsDto, tags: List[String])
case class ImageDto(full: String, group: String, h: Int, sprite: String, w: Int, x: Int, y: Int)

/** Map endpoint */
case class MapDetailsDto(image: ImageDto, mapId: Long, mapName: String, unpurchasableItemList: List[Long])
case class MapDataDto(data: Map[String, MapDetailsDto], mapDataType: String, version: String)

/** Realm endpoint */
case class RealmDto(cdn: String, css: String, dd: String, l: String, lg: String, n: Map[String, String],
                    profileiconmax: Int, store: String, v: String)