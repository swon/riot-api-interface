package com.rai.api

import akka.actor.ActorRef
import uk.co.robinmurphy.http.Response

case class ApiInputs(destination: ActorRef, courier: ActorRef, url: String, params: Map[String, String])
case class Parcel(destination: ActorRef, res: Response)