package com.rai.api

import akka.actor._
import akka.contrib.throttle.Throttler._
import akka.contrib.throttle.TimerBasedThrottler
import akka.util.Timeout
import com.typesafe.config.ConfigFactory
import uk.co.robinmurphy.http._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._


class RiotActor extends Actor {
  val sprayHttpClient = new SprayHttpClient

  def receive = {
    case ApiInputs(destination, courier, url, params) =>
      sprayHttpClient.get(url, params, Map[String, String]()).map { res => courier ! Parcel(destination, res) }
  }
}

object RiotRetriever {
  implicit val timeout = Timeout(10.seconds)
  val conf = ConfigFactory.load("riot.conf")
  val key = conf.getString("dev.key")

  val system = ActorSystem("riotApi")
  val riotActor = system.actorOf(Props[RiotActor])
  val throttler = system.actorOf(Props(classOf[TimerBasedThrottler], 1 msgsPer 2.second))
  throttler ! SetTarget(Some(riotActor))

  def getData(destination: ActorRef, courier: ActorRef, url: String, params: Map[String, String]) = {
    val msg = ApiInputs(destination, courier, url, params + ("api_key" -> key))
    throttler ! msg
  }
}
