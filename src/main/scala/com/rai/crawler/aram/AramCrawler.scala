package com.rai.crawler.aram

import akka.actor.{ActorSystem, Props}
import com.rai.api._
import com.rai.api.game._
import com.rai.api.lolmatch._
import com.sksamuel.elastic4s.ElasticDsl._


case class StartMessage()

class AramCrawler extends RiotApi {
  val system = ActorSystem("AramCrawler")
  val gameActor = system.actorOf(Props(new GameApi))
  val matchActor = system.actorOf(Props(new MatchApi))

  def receive = {
    case StartMessage => seedMatches()
    case games: RecentGamesDto => parseGames(games)
    case aramMatch: MatchDetail => parseMatch(aramMatch)
  }

  def parseGames(games: RecentGamesDto) = {
    games.games.foreach { game =>
      if (game.gameMode == "ARAM" && game.gameType == "MATCHED_GAME") {
        val resp = Option(esClient.execute { get id game.gameId from "league-of-legends/arams" }.await.getSource)
        resp getOrElse { matchActor ! GetMatch(game.gameId, includeTimeline = true) }
      }
      game.fellowPlayers.foreach { fellowPlayer =>
        gameActor ! GetRecentGames(fellowPlayer.summonerId)
      }
    }
  }

  def parseMatch(aramMatch: MatchDetail) = {
    /** @todo Fill in to save to ES later. See hack at MatchApi */
    println("Saved match " + aramMatch.matchId.toString)
  }

  def seedMatches() = {
    gameActor ! GetMyRecentGames()
  }
}

object AramCrawler {
  def main(args: Array[String]) {
    val system = ActorSystem("AramCrawler")
    val crawler = system.actorOf(Props(new AramCrawler))
    crawler ! StartMessage
  }
}
