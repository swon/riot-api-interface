package com.rai.summary.aram

import akka.actor.{ActorLogging, Actor, Props, ActorSystem}
import akka.util.Timeout
import com.rai.api.lolmatch._
import com.sksamuel.elastic4s.{ElasticsearchClientUri, ElasticClient}
import com.typesafe.config.ConfigFactory
import org.elasticsearch.action.search.SearchResponse
import org.elasticsearch.common.settings.ImmutableSettings
import org.json4s._
import org.json4s.jackson.JsonMethods._
import scala.concurrent.duration._


class StartingItemsActor extends Actor with ActorLogging {
  implicit val formats = org.json4s.DefaultFormats
  implicit val timeout = Timeout(5.seconds)

  val conf = ConfigFactory.load("riot.conf")
  val esConf = ConfigFactory.load("elasticsearch.conf")

  val key = conf.getString("dev.key")
  val region = conf.getString("dev.region")
  val mySummonerId = conf.getString("dev.summoner.id")

  val uri = ElasticsearchClientUri(conf.getString("es.address"))
  val settings = ImmutableSettings.settingsBuilder().put("cluster.name", conf.getString("es.cluster")).build()
  val esClient = ElasticClient.remote(settings, uri)
  val esActor = context.actorOf(Props[ElasticsearchActor], "ElasticsearchActor")

  val pots = List(2003, 2004, 2009, 2010)
  var recordsProcessed = 0

  def receive = {
    case StartMessage => esActor ! GetStartingArams
    case resp: SearchResponse => processStartingItems(resp)
  }

  def earliestKills(k1: ChampionKillEvent, k2: ChampionKillEvent): ChampionKillEvent =
    if(k1.timestamp < k2.timestamp) k1 else k2

  def processStartingItems(resp: SearchResponse) = {
    if (resp.getHits.getHits.size > 0) {
      resp.getHits.getHits.map { f =>
        val json = parse(f.getSourceAsString)
        val lolmatch = json.extract[MatchDetail]
        val winningTeam = lolmatch.teams.filter( t => t.winner ).head.teamId
        val participants = lolmatch.participants

        val events = lolmatch.timeline.map( t => t.frames.flatMap( frame => frame.events ) ).get
        val kills = events.filter( event => event.eventType == "CHAMPION_KILL" )
        val itemsPurchased = events.filter( event => event.eventType == "ITEM_PURCHASED" )
        val itemsLost = events.filter( event => event.eventType.matches("ITEM_DESTROYED|ITEM_SOLD|ITEM_UNDO") )

        participants.foreach { participant =>
          val participantDeaths = kills.filter( k => k.victimId.get == participant.participantId )
          if (participantDeaths.isEmpty) {
            val participantItemsPurchased = itemsPurchased.filter { event =>
              event.participantId.get == participant.participantId && !pots.contains(event.itemId.get)
            }.map( event => event.itemId.get )
            val participantItemsLost = itemsLost.filter { event =>
              event.participantId.get == participant.participantId &&
              ( if (event.eventType == "ITEM_UNDO") !pots.contains(event.itemBefore.get)
              else !pots.contains(event.itemId.get) )
            }.map( event => if (event.eventType == "ITEM_UNDO") event.itemBefore.get else event.itemId.get )
            val items = participantItemsPurchased filterNot participantItemsLost.contains
            val itemCounts = if (participant.teamId == winningTeam) {
              StartingItemsCounts(items.toList.sorted.map(_.toString.toInt), 1, 1)
            } else {
              StartingItemsCounts(items.toList.sorted.map(_.toString.toInt), 1, 0)
            }
            esActor ! new ChampionStartingItems(participant.championId, itemCounts)
          } else {
            val firstDeath = participantDeaths.min(Ordering.by((e: Event) => e.timestamp)).timestamp
            val participantItemsPurchased = itemsPurchased.filter { event =>
              event.participantId.get == participant.participantId &&
              !pots.contains(event.itemId.get) &&
              event.timestamp < firstDeath
            }.map( event => event.itemId.get )
            val participantItemsLost = itemsLost.filter { event =>
              event.participantId.get == participant.participantId &&
              ( if (event.eventType == "ITEM_UNDO") !pots.contains(event.itemBefore.get)
              else !pots.contains(event.itemId.get) ) &&
              event.timestamp < firstDeath
            }.map( event => if (event.eventType == "ITEM_UNDO") event.itemBefore.get else event.itemId.get )
            val items = participantItemsPurchased filterNot participantItemsLost.contains
            val itemCounts = if (participant.teamId == winningTeam) {
              StartingItemsCounts(items.toList.sorted.map(_.toString.toInt), 1, 1)
            } else {
              StartingItemsCounts(items.toList.sorted.map(_.toString.toInt), 1, 0)
            }
            esActor ! new ChampionStartingItems(participant.championId, itemCounts)
          }
        }
      }
      recordsProcessed += resp.getHits.getHits.size
      println("TETERI: Finished " + recordsProcessed.toString + " records.")
      esActor ! (self, resp.getScrollId)
    } else {
      /** @todo Call indexing here */
      context.system.shutdown()
    }
  }
}

object StartingItems {
  def main(args: Array[String]) {
    val system = ActorSystem("StartingItems")
    val itemsActor = system.actorOf(Props(new StartingItemsActor))
    itemsActor ! StartMessage
  }
}