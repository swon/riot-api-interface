package com.rai.api.lolmatch

case class Position(x: Option[Int], y: Option[Int])
case class ParticipantFrame(currentGold: Int, dominionScore: Int, jungleMinionsKilled: Int, level: Int,
                            minionsKilled: Int, participantId: Int, position: Position, teamScore: Int, totalGold: Int,
                            xp: Int)
case class Event(ascendedType: Option[String], assistingParticipantIds: Option[List[Int]], buildingType: Option[String],
                 creatorId: Option[Int], eventType: String, itemAfter: Option[Int], itemBefore: Option[Int],
                 itemId: Option[Int], killerId: Option[Int], laneType: Option[String], levelUpType: Option[String],
                 monsterType: Option[String], participantId: Option[Int], pointCaptured: Option[String],
                 position: Option[Position], skillSlot: Option[Int], teamId: Option[Int], timestamp: Long,
                 towerType: Option[String], victimId: Option[Int], wardType: Option[String])
case class ParticipantTimelineData(tenToTwenty: Double, thirtyToEnd: Double, twentyToThirty: Double, zeroToTen: Double)
case class Frame(events: List[Event], participantFrames: Map[String, ParticipantFrame], timestamp: Long)
case class BannedChampion(championId: Int, pickTurn: Int)
case class Player(matchHistoryUri: String, profileIcon: Int, summonerId: Long, summonerName: String)
case class Rune(rank: Long, runeId: Long)
case class ParticipantTimeline(ancientGolemAssistsPerMinCounts: Option[ParticipantTimelineData],
                               ancientGolemKillsPerMinCounts: Option[ParticipantTimelineData],
                               assistedLaneDeathsPerMinDeltas: Option[ParticipantTimelineData],
                               assistedLaneKillsPerMinDeltas: Option[ParticipantTimelineData],
                               baronAssistsPerMinCounts: Option[ParticipantTimelineData],
                               baronKillsPerMinCounts: Option[ParticipantTimelineData],
                               creepsPerMinDeltas: Option[ParticipantTimelineData],
                               csDiffPerMinDeltas: Option[ParticipantTimelineData],
                               damageTakenDiffPerMinDeltas: Option[ParticipantTimelineData],
                               damageTakenPerMinDeltas: Option[ParticipantTimelineData],
                               dragonAssistsPerMinCounts: Option[ParticipantTimelineData],
                               dragonKillsPerMinCounts: Option[ParticipantTimelineData],
                               elderLizardAssistsPerMinCounts: Option[ParticipantTimelineData],
                               elderLizardKillsPerMinCounts: Option[ParticipantTimelineData],
                               goldPerMinDeltas: Option[ParticipantTimelineData],
                               inhibitorAssistsPerMinCounts: Option[ParticipantTimelineData],
                               inhibitorKillsPerMinCounts: Option[ParticipantTimelineData],
                               lane: String, role: String,
                               towerAssistsPerMinCounts: Option[ParticipantTimelineData],
                               towerKillsPerMinCounts: Option[ParticipantTimelineData],
                               towerKillsPerMinDeltas: Option[ParticipantTimelineData],
                               vilemawAssistsPerMinCounts: Option[ParticipantTimelineData],
                               vilemawKillsPerMinCounts: Option[ParticipantTimelineData],
                               wardsPerMinDeltas: Option[ParticipantTimelineData],
                               xpDiffPerMinDeltas: Option[ParticipantTimelineData],
                               xpPerMinDeltas: Option[ParticipantTimelineData])
case class ParticipantStats(assists: Option[Long], champLevel: Option[Long], combatPlayerScore: Option[Long], deaths: Option[Long], doubleKills: Option[Long],
                            firstBloodAssist: Option[Boolean], firstBloodKill: Option[Boolean], firstInhibitorAssist: Option[Boolean],
                            firstInhibitorKill: Option[Boolean], firstTowerAssist: Option[Boolean], firstTowerKill: Option[Boolean],
                            goldEarned: Option[Long], goldSpent: Option[Long], inhibitorKills: Option[Long], item0: Option[Long], item1: Option[Long],
                            item2: Option[Long], item3: Option[Long], item4: Option[Long], item5: Option[Long], item6: Option[Long], killingSprees: Option[Long],
                            kills: Option[Long], largestCriticalStrike: Option[Long], largestKillingSpree: Option[Long], largestMultiKill: Option[Long],
                            magicDamageDealt: Option[Long], magicDamageDealtToChampions: Option[Long], magicDamageTaken: Option[Long],
                            minionsKilled: Option[Long], neutralMinionsKilled: Option[Long], neutralMinionsKilledEnemyJungle: Option[Long],
                            neutralMinionsKilledTeamJungle: Option[Long], nodeCapture:Option[Long], nodeCaptureAssist: Option[Long],
                            nodeNeutralize: Option[Long], nodeNeutralizeAssist: Option[Long], objectivePlayerScore: Option[Long],
                            pentaKills: Option[Long], physicalDamageDealt: Option[Long], physicalDamageDealtToChampions: Option[Long],
                            physicalDamageTaken: Option[Long], quadraKills: Option[Long], sightWardsBoughtInGame: Option[Long],
                            teamObjective: Option[Long], totalDamageDealt: Option[Long], totalDamageDealtToChampions: Option[Long],
                            totalDamageTaken: Option[Long], totalHeal: Option[Long], totalPlayerScore: Option[Long], totalScoreRank: Option[Long],
                            totalTimeCrowdControlDealt: Option[Long], totalUnitsHealed: Option[Long], towerKills: Option[Long],
                            tripleKills: Option[Long], trueDamageDealt: Option[Long], trueDamageDealtToChampions: Option[Long],
                            trueDamageTaken: Option[Long], unrealKills: Option[Long], visionWardsBoughtInGame: Option[Long], wardsKilled: Option[Long],
                            wardsPlaced: Option[Long], winner: Option[Boolean])
case class Mastery(masteryId: Long, rank: Long)
case class Timeline(frameInterval: Long, frames: List[Frame])
case class Team(bans: List[BannedChampion], baronKills: Int, dominionVictoryScore: Long, dragonKills: Int,
                firstBaron: Boolean, firstBlood: Boolean, firstDragon: Boolean, firstInhibitor: Boolean,
                firstTower: Boolean, inhibitorKills: Int, teamId: Int, towerKills: Int, vilemawKills: Int,
                winner: Boolean)
case class ParticipantIdentity(participantId: Int, player: Option[Player])
case class Participant(championId: Int, highestAchievedSeasonTier: String, masteries: List[Mastery], participantId: Int,
                       runes: List[Rune], spell1Id: Int, spell2Id: Int, stats: ParticipantStats, teamId: Int,
                       timeline: ParticipantTimeline)
case class MatchDetail(mapId: Int, matchCreation: Long, matchDuration: Long, matchId: Long, matchMode: String,
                       matchType: String, matchVersion: String, participantIdentities: List[ParticipantIdentity],
                       participants: List[Participant], platformId: String, queueType: String, region: String,
                       season: String, teams: List[Team], timeline: Option[Timeline])
