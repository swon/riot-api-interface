package com.rai.api.status

case class Translation(content: String, locale: String, updated_at: String)
case class Message(author: String, content: String, created_at: String, id: Long, severity: String,
                   translations: List[Translation], updated_at: String)
case class Incident(active: Boolean, created_at: String, id: Long, updates: List[Message])
case class Service(incidents: List[Incident], name: String, slug: String, status: String)
case class ShardStatus(hostname: String, locales: List[String], name: String, region_tag: String,
                       services: List[Service], slug: String)
case class Shard(hostname: String, locales: List[String], name: String, region_tag: String, slug: String)
