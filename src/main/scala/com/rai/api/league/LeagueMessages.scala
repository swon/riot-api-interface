package com.rai.api.league

import com.rai.api.MatchTypes._


/** NOTE: I know that the following messages are in the Riot API.
  * However, I don't really have a need for them. If someone needs them,
  * contact me @ the Riot API forums (Randomodnar)
  */
//case class GetLeaguesBySummoner(summonerIds: List[Long])
//case class GetLeagueEntriesBySummoner(summonerIds: List[Long])
//case class GetLeaguesByTeam(teamIds: List[Long])
//case class GetLeagueEntriesByTeam(teamIds: List[Long])

/** Valid values are RANKED_SOLO_5x5, RANKED_TEAM_3x3, RANKED_TEAM_5x5 */
case class GetMasterLeague(leagueType: MatchTypes = RANKED_SOLO_5X5)
case class GetChallengerLeague(leagueType: MatchTypes = RANKED_SOLO_5X5)
