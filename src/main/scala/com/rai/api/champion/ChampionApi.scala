package com.rai.api.champion

import akka.actor.ActorRef
import com.rai.api.{RiotRetriever, Parcel, RiotApi}
import org.json4s._
import org.json4s.jackson.JsonMethods._
import uk.co.robinmurphy.http.Response


class ChampionApi extends RiotApi {
  implicit val formats = org.json4s.DefaultFormats
  val championUrl = baseUri + "/api/lol/" + region + "/v1.2/champion"

  def receive = {
    case GetAll(freeToPlay) => getAll(freeToPlay)
    case GetById(champ) => getById(champ)
    case Parcel(destination, res) => returnResponse(destination, res)
  }

  def returnResponse(destination: ActorRef, res: Response) = {
    val json = parse(res.body)
    json.extractOpt[ChampionListDto].foreach { destination ! _ }
    json.extractOpt[ChampionDto].foreach { destination ! _ }
  }

  def getAll(freeToPlay: Boolean) = {
    val url = championUrl
    if (freeToPlay)
      params += ("freeToPlay" -> "true")
    RiotRetriever.getData(sender, self, url, params)
  }

  def getById(id: Int) = {
    val url = championUrl + "/" + id.toString
    RiotRetriever.getData(sender, self, url, params)
  }
}
