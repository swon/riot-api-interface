package com.rai.api.matchlist

import com.rai.api.RankedMatchTypes._
import com.rai.api.Seasons._

case class GetMatchlist(summonerId: Long, championIds: List[Int] = List(),
                        rankedQueues: List[RankedMatchTypes] = List(), seasons: List[Seasons] = List())