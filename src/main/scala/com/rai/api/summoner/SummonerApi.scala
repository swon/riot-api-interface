package com.rai.api.summoner

import akka.actor.ActorRef
import com.rai.api.{RiotRetriever, Parcel, RiotApi}
import org.json4s._
import org.json4s.jackson.JsonMethods._
import uk.co.robinmurphy.http.Response

class SummonerApi extends RiotApi {
  implicit val formats = org.json4s.DefaultFormats
  val summonerUrl = baseUri + "/api/lol/" + region + "/v1.4/summoner/"

  def receive = {
    case GetObjectsByName(summonerNames) => getObjectsByName(summonerNames)
    case GetObjectsById(summonerIds) => getObjectsById(summonerIds)
    case GetSummonerMasteries(summonerIds) => getMasteries(summonerIds)
    case GetNames(summonerIds) => getNames(summonerIds)
    case GetSummonerRunes(summonerIds) => getRunes(summonerIds)
    case Parcel(destination, res) => returnResponse(destination, res)
  }

  def returnResponse(destination: ActorRef, res: Response) = {
    val json = parse(res.body)
    json.extractOpt[Map[String, SummonerDto]].foreach { destination ! _ }
    json.extractOpt[Map[String, MasteryPagesDto]].foreach { destination ! _ }
    json.extractOpt[Map[String, String]].foreach { destination ! _ }
    json.extractOpt[Map[String, RunePagesDto]].foreach { destination ! _ }
  }

  def getObjectsByName(summonerNames: List[String]) = {
    val url = summonerUrl + "by-name/" + summonerNames.mkString(",")
    RiotRetriever.getData(sender, self, url, params)
  }

  def getObjectsById(summonerIds: List[Long]) = {
    val url = summonerUrl + summonerIds.mkString(",")
    RiotRetriever.getData(sender, self, url, params)
  }

  def getMasteries(summonerIds: List[Long]) = {
    val url = summonerUrl + summonerIds.mkString(",") + "/masteries"
    RiotRetriever.getData(sender, self, url, params)
  }

  def getNames(summonerIds: List[Long]) = {
    val url = summonerUrl + summonerIds.mkString(",") + "/name"
    RiotRetriever.getData(sender, self, url, params)
  }

  def getRunes(summonerIds: List[Long]) = {
    val url = summonerUrl + summonerIds.mkString(",") + "/runes"
    RiotRetriever.getData(sender, self, url, params)
  }
}
