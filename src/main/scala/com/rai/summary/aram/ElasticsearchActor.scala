package com.rai.summary.aram

import java.util.HashMap

import akka.actor.{ActorRef, Actor, ActorLogging}
import com.sksamuel.elastic4s.ElasticDsl._
import com.sksamuel.elastic4s.mappings.FieldType._
import com.sksamuel.elastic4s.{ElasticClient, ElasticsearchClientUri}
import com.typesafe.config.ConfigFactory
import org.elasticsearch.common.settings.ImmutableSettings
import org.json4s._
import org.json4s.jackson.JsonMethods._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}


case class GetStartingArams()

class ElasticsearchActor extends Actor with ActorLogging {
  implicit val formats = org.json4s.DefaultFormats
  import scala.collection.JavaConverters._

  val conf = ConfigFactory.load("riot.conf")
  val esConf = ConfigFactory.load("elasticsearch.conf")

  val key = conf.getString("dev.key")
  val region = conf.getString("dev.region")
  val mySummonerId = conf.getString("dev.summoner.id")

  val uri = ElasticsearchClientUri(conf.getString("es.address"))
  val settings = ImmutableSettings.settingsBuilder().put("cluster.name", conf.getString("es.cluster")).build()
  val esClient = ElasticClient.remote(settings, uri)

  def receive = {
    case GetStartingArams => getStartingScroll()
    case (actor: ActorRef, championId: Int) => getChampionData(actor, championId)
    case (actor: ActorRef, scrollId: String) => getMoreResults(actor, scrollId)
    case items: ChampionStartingItems => updateCounts(items)
  }

  def getStartingScroll() = {
    /** Get starting scroll */
    val resp = esClient.execute {
      search in "league-of-legends" / "arams" query { matchQuery("matchType", "MATCHED_GAME") } scroll "1m" limit 100
    }.await

    /** Create the index mapping */
    val indexStatus = esClient.execute { index exists "summary" }.await
    if (!indexStatus.isExists) {
      val req = create.index("summary").mappings(
        "champions" as (
          "name" typed StringType,
          "count" typed LongType,
          "winner" typed LongType,
          "matchDurationWin" typed LongType,
          "matchDurationWin2" typed LongType,
          "matchDurationLose" typed LongType,
          "matchDurationLose2" typed LongType,
          "masteries" typed NestedType as (
            "name" typed StringType,
            "masteryTree" typed StringType,
            "count" typed LongType,
            "winner" typed LongType
          ),
          "runes" typed NestedType as (
            "name" typed StringType,
            "count" typed LongType,
            "winner" typed LongType
          ),
          "summoners" typed NestedType as (
            "name" typed StringType,
            "count" typed LongType,
            "winner" typed LongType
          ),
          "items" typed NestedType as (
            "name" typed StringType,
            "count" typed LongType,
            "winner" typed LongType
          ),
          "starting_items" typed NestedType as (
            "items" typed IntegerType,
            "names" typed StringType,
            "count" typed LongType,
            "winner" typed LongType
          ),
          /** Should represent skill order 1-16, 1st one is levels 1-3 */
          "skill_order" typed NestedType as (
            "count" typed LongType,  /** 4-element array for each skillSlot */
            "winner" typed LongType
          ),
          "allies" typed NestedType as (
            "name" typed StringType,
            "count" typed LongType,
            "winner" typed LongType
          ),
          "enemies" typed NestedType as (
            "name" typed StringType,
            "count" typed LongType,
            "winner" typed LongType
          )
        )
      )
      esClient.execute {
        req
      }.await
    }

    /** Send back the scroll results */
    sender ! resp
  }

  def getMoreResults(actor: ActorRef, scrollId: String) ={
    val future = esClient.execute {
      searchScroll(scrollId).keepAlive("3m")
    }
    future.onComplete {
      case Success(res) => actor ! res
      case Failure(ex) => println("Error: ${ex.getMessage}")
    }
  }

  def getChampionData(actor: ActorRef, championId: Int) ={
    val resp = esClient.execute {
      get id championId from "summary/champions"
    }
    resp.onComplete {
      case Success(res) =>
        val json = parse(res.getSourceAsString)
        val startingItems =
          json.extract[Map[String, Map[Long, StartingItemsCounts]]].get("starting_items").get.values.toList
        actor ! startingItems.filter(s => s.count > 10).sortWith { (ls, rs) =>
          (ls.winner.toFloat / ls.count) > (rs.winner.toFloat / rs.count)
        }
      case Failure(ex) => println("Failed to get champion from summary")
    }
  }

  def updateCounts(items: ChampionStartingItems) = {
    val startingItemsHash = items.startingItems.items.hashCode.toString
    val resp = esClient.execute {
      get id items.id from "summary/champions"
    }
    resp onComplete {
      case Success(res) =>
        val sourceMap = Option(res.getSourceAsMap)
        sourceMap match {
          case Some(c) =>
            val startingItems = c.get("starting_items").asInstanceOf[java.util.HashMap[String, Object]].asScala
            startingItems.get(startingItemsHash) match {
              case Some(i: HashMap[String, Any]) =>
                val updatedCount = items.startingItems.count + i.get("count").asInstanceOf[Integer]
                val updatedWinner = items.startingItems.winner + i.get("winner").asInstanceOf[Integer]
                esClient.execute {
                  update(items.id).in("summary/champions") docAsUpsert (
                    "starting_items" -> Map(startingItemsHash -> Map(
                      "items" -> items.startingItems.items,
                      "count" -> updatedCount,
                      "winner" -> updatedWinner
                    ))
                  ) retryOnConflict 10
                }
              case None =>
                esClient.execute {
                  update(items.id).in("summary/champions") docAsUpsert (
                    "starting_items" -> Map(startingItemsHash -> Map(
                      "items" -> items.startingItems.items,
                      "count" -> items.startingItems.count,
                      "winner" -> items.startingItems.winner
                    ))
                  ) retryOnConflict 10
                }
            }
          case None =>
            esClient.execute {
              index into "summary" -> "champions" id items.id fields (
                "starting_items" -> Map(startingItemsHash -> Map(
                  "items" -> items.startingItems.items,
                  "count" -> items.startingItems.count,
                  "winner" -> items.startingItems.winner
                ))
              )
            }.await
        }
      case Failure(t) => println("Error occurred when getting champion: " + t.toString)
    }
  }
}
