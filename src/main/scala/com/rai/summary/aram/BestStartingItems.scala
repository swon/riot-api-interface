package com.rai.summary.aram

import akka.actor.{ActorLogging, Actor, Props, ActorSystem}
import akka.util.Timeout
import com.rai.api.lolmatch._
import com.sksamuel.elastic4s.{ElasticsearchClientUri, ElasticClient}
import com.typesafe.config.ConfigFactory
import org.elasticsearch.action.search.SearchResponse
import org.elasticsearch.common.settings.ImmutableSettings
import org.json4s._
import org.json4s.jackson.JsonMethods._
import scala.concurrent.duration._


class BestStartingItemsActor extends Actor with ActorLogging {
  implicit val formats = org.json4s.DefaultFormats
  implicit val timeout = Timeout(5.seconds)

  val conf = ConfigFactory.load("riot.conf")
  val esConf = ConfigFactory.load("elasticsearch.conf")

  val key = conf.getString("dev.key")
  val region = conf.getString("dev.region")
  val mySummonerId = conf.getString("dev.summoner.id")

  val uri = ElasticsearchClientUri(conf.getString("es.address"))
  val settings = ImmutableSettings.settingsBuilder().put("cluster.name", conf.getString("es.cluster")).build()
  val esClient = ElasticClient.remote(settings, uri)
  val esActor = context.actorOf(Props[ElasticsearchActor], "ElasticsearchActor")

  def receive = {
    case championId: Int => esActor ! (self, championId)
    case items: List[StartingItemsCounts] => printBestItems(items)
  }

  def printBestItems(items: List[StartingItemsCounts]) = {
    items.take(10).foreach { item =>
      println(item.toString)
    }
  }
}

object BestStartingItems {
  def main(args: Array[String]) {
    val system = ActorSystem("BestStartingItems")
    val itemsActor = system.actorOf(Props(new BestStartingItemsActor))
    itemsActor ! 91
  }
}