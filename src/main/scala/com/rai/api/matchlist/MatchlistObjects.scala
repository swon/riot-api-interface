package com.rai.api.matchlist

case class MatchReference(champion: Long, lane: String, matchId: Long, platformId: String, queue: String,
                          region: String, role: String, season: String, timestamp: Long)
case class MatchList(endIndex: Int, matches: List[MatchReference], startIndex: Int, totalGames: Int)