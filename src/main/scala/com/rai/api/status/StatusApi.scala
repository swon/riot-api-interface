package com.rai.api.status

import akka.actor.ActorRef
import com.rai.api.{RiotRetriever, Parcel, RiotApi}
import org.json4s._
import org.json4s.jackson.JsonMethods._
import uk.co.robinmurphy.http.Response

class StatusApi extends RiotApi {
  implicit val formats = org.json4s.DefaultFormats
  val statusUrl = "http://status.leagueoflegends.com/shards"

  def receive = {
    case GetStatus => getStatus
    case GetStatusFor(region) => getStatusFor(region)
    case Parcel(destination, res) => returnResponse(destination, res)
  }

  def returnResponse(destination: ActorRef, res: Response) = {
    val json = parse(res.body)
    json.extractOpt[List[Shard]].foreach { destination ! _ }
    json.extractOpt[ShardStatus].foreach { destination ! _ }
  }

  def getStatus = {
    val url = statusUrl
    RiotRetriever.getData(sender, self, url, params)
  }

  def getStatusFor(region: String) = {
    val url = statusUrl + "/" + region
    RiotRetriever.getData(sender, self, url, params)
  }
}