package com.rai.api.stats

import akka.actor.ActorRef
import com.rai.api.{RiotRetriever, Parcel, RiotApi}
import org.json4s._
import org.json4s.jackson.JsonMethods._
import uk.co.robinmurphy.http.Response

class StatsApi extends RiotApi {
  implicit val formats = org.json4s.DefaultFormats
  val statsUrl = baseUri + "/api/lol/" + region + "/v1.3/stats/by-summoner/"

  def receive = {
    case GetRankedFor(summonerId, season) => getRankedFor(summonerId, season)
    case GetSummaryFor(summonerId, season) => getSummaryFor(summonerId, season)
    case Parcel(destination, res) => returnResponse(destination, res)
  }

  def returnResponse(destination: ActorRef, res: Response) = {
    val json = parse(res.body)
    json.extractOpt[RankedStatsDto].foreach { destination ! _ }
    json.extractOpt[PlayerStatsSummaryListDto].foreach { destination ! _ }
  }

  def getRankedFor(summonerId: Long, season: String) = {
    val url = statsUrl + summonerId.toString + "/ranked"
    params += ("season" -> season)
    RiotRetriever.getData(sender, self, url, params)
  }

  def getSummaryFor(summonerId: Long, season: String) = {
    val url = statsUrl + summonerId.toString + "/summary"
    params += ("season" -> season)
    RiotRetriever.getData(sender, self, url, params)
  }
}