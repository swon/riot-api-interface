package com.rai.api.team

case class TeamMemberInfoDto(inviteDate: Long, joinDate: Long, playerId: Long, status: String)
case class TeamStatDetailDto(averageGamesPlayed: Int, losses: Int, teamStatType: String, wins: Int)
case class RosterDto(memberList: List[TeamMemberInfoDto], ownerId: Long)
case class MatchHistorySummaryDto(assists: Int, date: Long, deaths: Int, gameId: Long, gameMode: String,
                                  invalid: Boolean, kills: Int, mapId: Int, opposingTeamKills: Int,
                                  opposingTeamName: String, win: Boolean)
case class TeamDto(createDate: Long, fullId: String, lastGameDate: Long, lastJoinDate: Long,
                   lastJoinedRankedTeamQueueDate: Long, matchHistory: List[MatchHistorySummaryDto],
                   modifyDate: Long, name: String, roster: RosterDto, secondLastJoinDate: Long, status: String,
                   tag: String, teamStatDetails: List[TeamStatDetailDto], thirdLastJoinDate: Long)
