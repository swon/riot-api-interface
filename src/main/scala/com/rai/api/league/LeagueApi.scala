package com.rai.api.league

import akka.actor.ActorRef
import com.rai.api.MatchTypes.MatchTypes
import com.rai.api.{RiotRetriever, Parcel, RiotApi}
import org.json4s._
import org.json4s.jackson.JsonMethods._
import uk.co.robinmurphy.http.Response


class LeagueApi extends RiotApi {
  implicit val formats = org.json4s.DefaultFormats
  val leagueUrl = baseUri + "/api/lol/" + region + "/v2.5/league/"

  def receive = {
    case GetChallengerLeague(leagueType) => getChallengerLeague(leagueType)
    case GetMasterLeague(leagueType) => getMasterLeague(leagueType)
    case Parcel(destination, res) => returnResponse(destination, res)
  }

  def returnResponse(destination: ActorRef, res: Response) = {
    val json = parse(res.body)
    json.extractOpt[LeagueDto].foreach { destination ! _ }
  }

  def getLeague(league: String, leagueType: MatchTypes) = {
    val url = leagueUrl + league
    /** Need to switch to String */
    params += ("type" -> leagueType.toString)
    RiotRetriever.getData(sender, self, url, params)
  }

  def getMasterLeague(leagueType: MatchTypes) = getLeague("master", leagueType)

  def getChallengerLeague(leagueType: MatchTypes) = getLeague("challenger", leagueType)
}
