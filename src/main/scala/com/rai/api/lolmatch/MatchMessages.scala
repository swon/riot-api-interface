package com.rai.api.lolmatch

case class GetMatch(matchId: Long, includeTimeline: Boolean = false)
