package com.rai.api.featuredgames

import akka.actor.ActorRef
import com.rai.api.{RiotRetriever, Parcel, RiotApi}
import org.json4s._
import org.json4s.jackson.JsonMethods._
import uk.co.robinmurphy.http.Response


class FeaturedGamesApi extends RiotApi {
  implicit val formats = org.json4s.DefaultFormats

  def receive = {
    case GetFeaturedGames => getFeaturedGames
    case Parcel(destination, res) => returnResponse(destination, res)
  }

  def returnResponse(destination: ActorRef, res: Response) = {
    val json = parse(res.body)
    json.extractOpt[FeaturedGames].foreach { destination ! _ }
  }

  def getFeaturedGames() = {
    val url = baseUri + "/observer-mode/rest/featured"
    RiotRetriever.getData(sender, self, url, params)
  }
}
