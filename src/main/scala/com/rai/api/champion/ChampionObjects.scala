package com.rai.api.champion

case class ChampionDto(active: Boolean, botEnabled: Boolean, botMmEnabled: Boolean,
                       freeToPlay: Boolean, id: Long, rankedPlayEnabled: Boolean)

case class ChampionListDto(champions: List[ChampionDto])