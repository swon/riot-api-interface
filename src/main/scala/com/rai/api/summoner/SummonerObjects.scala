package com.rai.api.summoner

case class RuneSlotDto(runeId: Int, runeSlotId: Int)
case class RunePageDto(current: Boolean, id: Long, name: String, slots: Set[RuneSlotDto])
case class RunePagesDto(pages: Set[RunePageDto], summonerId: Long)
case class MasteryDto(id: Int, rank: Int)
case class MasteryPageDto(current: Boolean, id: Long, masteries: List[MasteryDto], name: String)
case class MasteryPagesDto(pages: Set[MasteryPageDto], summonerId: Long)
case class SummonerDto(id: Long, name: String, profileIconId: Int, revisionDate: Long, summonerLevel: Long)
