package com.rai.api.team

case class GetTeamsBySummoner(summonerIds: List[Long])
case class GetTeams(teamIds: List[Long])
