package com.rai.api.staticdata

/** if (spellData != 'all') then {id, key, name, description, summonerLevel} */
/** @note 'effect': List of List of Double - may contain Nulls */
/** @note 'range': Either a List of Integer or the String 'self' for spells that target one's own champion */
case class SummonerSpellDto(cooldown: Option[List[Double]], cooldownBurn: Option[String], cost: Option[List[Int]],
                            costBurn: Option[String], costType: Option[String], description: String,
                            effect: Option[List[Option[List[Double]]]], effectBurn: Option[List[String]], id: Int,
                            image: Option[ImageDto], key: String, leveltip: Option[LevelTipDto], maxrank: Option[Int],
                            modes: Option[List[String]], name: String, range: Option[Either[List[Int], String]],
                            rangeBurn: Option[String], resource: Option[String], sanitizedDescription: Option[String],
                            sanitizedTooltip: Option[String], summonerLevel: Int, tooltip: Option[String],
                            vars: Option[List[SpellVarsDto]])
case class SummonerSpellListDto(data: Map[String, SummonerSpellDto], summonerSpellType: String, version: String)