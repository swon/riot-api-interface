package com.rai.api.matchlist

import com.rai.api.RankedMatchTypes._
import com.rai.api.Seasons._
import akka.actor.ActorRef
import com.rai.api.{RiotRetriever, Parcel, RiotApi}
import org.json4s._
import org.json4s.jackson.JsonMethods._
import uk.co.robinmurphy.http.Response


class MatchlistApi extends RiotApi {
  implicit val formats = org.json4s.DefaultFormats

  def receive = {
    case GetMatchlist(summonerId, championIds, rankedQueues, seasons) =>
      getMatchlist(summonerId, championIds, rankedQueues, seasons)
    case Parcel(destination, res) => returnResponse(destination, res)
  }

  def returnResponse(destination: ActorRef, res: Response) = {
    val json = parse(res.body)
    json.extractOpt[MatchList].foreach { destination ! _ }
  }

  def getMatchlist(summonerId: Long, championIds: List[Int], rankedQueues: List[RankedMatchTypes],
                   seasons: List[Seasons]) = {
    val url = baseUri + "/api/lol/" + region + "/v2.2/matchlist/by-summoner/" + summonerId.toString
    if (championIds.nonEmpty)
      params += ("championIds" -> championIds.mkString(","))
    if (rankedQueues.nonEmpty)
      params += ("rankedQueues" -> rankedQueues.mkString(","))
    if (seasons.nonEmpty)
      params += ("seasons" -> seasons.mkString(","))
    RiotRetriever.getData(sender, self, url, params)
  }
}