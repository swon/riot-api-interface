package com.rai.api.staticdata

/** if (runeListData != 'all') then {id, name, rune, description} */
case class RuneDto(colloq: Option[String], consumeOnFull: Option[Boolean], consumed: Option[Boolean],
                   depth: Option[Int], description: String, from: Option[List[String]], group: Option[String],
                   hideFromAll: Option[Boolean], id: Int, image: Option[ImageDto], inStore: Option[Boolean],
                   into: Option[List[String]], maps: Option[Map[String, Boolean]], name: String,
                   plaintext: Option[String], requiredChampion: Option[String], rune: MetaDataDto,
                   sanitizedDescription: Option[String], specialRecipe: Option[Int], stacks: Option[Int],
                   stats: Option[BasicDataStatsDto], tags: Option[List[String]])

/** if (runeListData != 'all') then {type, version, data} */
case class RuneListDto(basic: Option[BasicDataDto], data: Map[String, RuneDto], runeType: String, version: String)