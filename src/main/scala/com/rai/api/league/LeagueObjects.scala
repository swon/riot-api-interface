package com.rai.api.league

case class MiniSeriesDto(losses: Int, progress: String, target: Int, wins: Int)
case class LeagueEntryDto(division: String, isFreshBlood: Boolean, isHotStreak: Boolean, isInactive: Boolean,
                          isVeteran: Boolean, leaguePoints: Int, losses: Int, miniSeries: MiniSeriesDto,
                          playerOrTeamId: String, playerOrTeamName: String, wins: Int)
case class LeagueDto(entries: List[LeagueEntryDto], name: String, participantId: String, queue: String, tier: String)