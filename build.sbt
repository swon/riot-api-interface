name := "riot-api-interface"

version := "0.0.2-SNAPSHOT"

scalaVersion := "2.11.7"

ivyScala := ivyScala.value map { _.copy(overrideScalaVersion = true) }

val akkaVersion = "2.3.14"

resolvers ++= Seq(
  "spray repo" at "http://repo.spray.io/"
)

libraryDependencies ++= Seq(
  "com.sksamuel.elastic4s" %% "elastic4s-core" % "1.7.4",
  "com.sksamuel.elastic4s" %% "elastic4s-jackson" % "1.7.4",
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-agent" % akkaVersion,
  "com.typesafe.akka" %% "akka-contrib" % akkaVersion,
  "com.typesafe.play" %% "play-json" % "2.4.3",
  "io.spray" %% "spray-client" % "1.3.3",
  "org.elasticsearch" % "elasticsearch" % "1.7.1" % "provided",
  "org.elasticsearch" %% "elasticsearch-spark" % "2.1.1",
  "org.json4s" %% "json4s-jackson" % "3.2.11",
  "org.specs2" %% "specs2-core" % "3.6.4" % "test"
)

scalacOptions in Test ++= Seq("-Yrangepos")