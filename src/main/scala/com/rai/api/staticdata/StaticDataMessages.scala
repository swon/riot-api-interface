package com.rai.api.staticdata

case class GetChampions(dataById: Boolean = false, version: String = "5.17.1", champData: Boolean = false)
case class GetChampionById(id: Int, version: String = "5.17.1", champData: Boolean = false)
case class GetItems(version: String = "5.17.1", itemListData: Boolean = false)
case class GetItemById(id: Int, version: String = "5.17.1", itemData: Boolean = false)
case class GetMap(version: String = "5.17.1")
case class GetMasteries(version: String = "5.17.1", masteryListData: Boolean = false)
case class GetMasteryById(id: Int, version: String = "5.17.1", masteryData: Boolean = false)
case class GetRealm()  /** Could use to get current DDragon version */
case class GetRunes(version: String = "5.17.1", runeListData: Boolean = false)
case class GetRuneById(id: Int, version: String = "5.17.1", runeData: Boolean = false)
case class GetSummonerSpells(dataById: Boolean = false, version: String = "5.17.1", spellData: Boolean = false)
case class GetSummonerSpellById(id: Int, version: String = "5.17.1", spellData: Boolean = false)
case class GetVersions()