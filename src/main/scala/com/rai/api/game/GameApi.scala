package com.rai.api.game

import akka.actor.ActorRef
import com.rai.api.{RiotRetriever, Parcel, RiotApi}
import org.json4s._
import org.json4s.jackson.JsonMethods._
import uk.co.robinmurphy.http.Response


class GameApi extends RiotApi {
  implicit val formats = org.json4s.DefaultFormats

  def receive = {
    case GetMyRecentGames() => getMyRecentGames()
    case GetRecentGames(summonerId) => getRecentGames(summonerId)
    case Parcel(destination, res) => returnResponse(destination, res)
  }

  def returnResponse(destination: ActorRef, res: Response) = {
    val json = parse(res.body)
    json.extractOpt[RecentGamesDto].foreach { destination ! _ }
  }

  def getGames(summonerId: String) = {
    val url = baseUri + "/api/lol/" + region + "/v1.3/game/by-summoner/" + summonerId + "/recent"
    RiotRetriever.getData(sender, self, url, params)
  }

  def getMyRecentGames() = getGames(mySummonerId)

  def getRecentGames(summonerId: Long) = getGames(summonerId.toString)
}
