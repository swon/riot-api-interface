package com.rai.api.game

case class RawStatsDto(assists: Option[Int], barracksKilled: Option[Int], championsKilled: Option[Int], combatPlayerScore: Option[Int],
                       consumablesPurchased: Option[Int], damageDealtPlayer: Option[Int], doubleKills: Option[Int], firstBlood: Option[Int], gold: Option[Int],
                       goldEarned: Option[Int], goldSpent: Option[Int], item0: Option[Int], item1: Option[Int], item2: Option[Int], item3: Option[Int], item4: Option[Int],
                       item5: Option[Int], item6: Option[Int], itemsPurchased: Option[Int], killingSprees: Option[Int], largestCriticalStrike: Option[Int],
                       largestKillingSpree: Option[Int], largestMultiKill: Option[Int], legendaryItemsCreated: Option[Int], level: Option[Int],
                       magicDamageDealtPlayer: Option[Int], magicDamageDealtToChampions: Option[Int], magicDamageTaken: Option[Int],
                       minionsDenied: Option[Int], minionsKilled: Option[Int], neutralMinionsKilled: Option[Int],
                       neutralMinionsKilledEnemyJungle: Option[Int], neutralMinionsKilledYourJungle: Option[Int], nexusKilled: Option[Boolean],
                       nodeCapture: Option[Int], nodeCaptureAssist: Option[Int], nodeNeutralize: Option[Int], nodeNeutralizeAssist: Option[Int],
                       numDeaths: Option[Int], numItemsBought: Option[Int], objectivePlayerScore: Option[Int], pentaKills: Option[Int],
                       physicalDamageDealtPlayer: Option[Int], physicalDamageDealtToChampions: Option[Int], physicalDamageTaken: Option[Int],
                       playerPosition: Option[Int], playerRole: Option[Int], quadraKills: Option[Int], sightWardsBought: Option[Int], spell1Cast: Option[Int],
                       spell2Cast: Option[Int], spell3Cast: Option[Int], spell4Cast: Option[Int], summonSpell1Cast: Option[Int], summonSpell2Cast: Option[Int],
                       superMonsterKilled: Option[Int], team: Option[Int], teamObjective: Option[Int], timePlayed: Option[Int], totalDamageDealt: Option[Int],
                       totalDamageDealtToChampions: Option[Int], totalDamageTaken: Option[Int], totalHeal: Option[Int], totalPlayerScore: Option[Int],
                       totalScoreRank: Option[Int], totalTimeCrowdControlDealt: Option[Int], totalUnitsHealed: Option[Int], tripleKills: Option[Int],
                       trueDamageDealtPlayer: Option[Int], trueDamageDealtToChampions: Option[Int], trueDamageTaken: Option[Int],
                       turretsKilled: Option[Int], unrealKills: Option[Int], victoryPointTotal: Option[Int], visionWardsBought: Option[Int],
                       wardKilled: Option[Int], wardPlaced: Option[Int], win: Option[Boolean])
case class PlayerDto(championId: Int, summonerId: Long, teamId: Int)
case class GameDto(championId: Int, createDate: Long, fellowPlayers: List[PlayerDto], gameId: Long, gameMode: String,
                   gameType: String, invalid: Boolean, ipEarned: Int, level: Int, mapId: Int, spell1: Int, spell2: Int,
                   stats: RawStatsDto, subType: String, teamId: Int)
case class RecentGamesDto(games: Set[GameDto], summonerId: Long)
