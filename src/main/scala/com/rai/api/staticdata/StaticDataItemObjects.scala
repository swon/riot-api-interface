package com.rai.api.staticdata

case class ItemTreeDto(header: String, tags: List[String])
case class GroupDto(MaxGroupOwnable: String, key: String)

/** @note if (itemListData != 'all') then {id, name, plaintext, group, and description} */
case class ItemDto(colloq: Option[String], consumeOnFull: Option[Boolean], consumed: Option[Boolean],
                   depth: Option[Int], description: String, effect: Option[Map[String, String]],
                   from: Option[List[String]], gold: Option[GoldDto], group: String, hideFromAll: Option[Boolean],
                   id: Int, image: Option[ImageDto], inStore: Option[Boolean], into: Option[List[String]],
                   maps: Option[Map[String, Boolean]], name: String, plaintext: String,
                   requiredChampion: Option[String], rune: Option[MetaDataDto], sanitizedDescription: Option[String],
                   specialRecipe: Option[Int], stacks: Option[Int], stats: Option[BasicDataStatsDto],
                   tags: Option[List[String]])

/** @note if (itemListData != 'all') then {type, version, basic, data} */
case class ItemListDto(basic: BasicDataDto, data: Map[String, ItemDto], groups: Option[List[GroupDto]],
                       tree: Option[List[ItemTreeDto]], itemListType: String, version: String)
