package com.rai.api.stats

case class GetRankedFor(summonerId: Long, season: String = "SEASON2015")
case class GetSummaryFor(summonerId: Long, season: String = "SEASON2015")
